const resolve = require('rollup-plugin-node-resolve');
const external = ['hyperhtml'];

export default [
  {
    input: 'src/index.js',
    plugins: [],
    output: {
      file: 'dist/rollup-lib-3.mjs',
      format: 'es'
    }
  },
  {
    input: 'src/index.js',
    plugins: [resolve()],
    output: {
      file: 'dist/rollup-lib-3.bunlded.mjs',
      format: 'es'
    }
  }
];
