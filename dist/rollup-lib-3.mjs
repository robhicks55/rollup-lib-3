import { hyper } from 'hyperhtml';

/**
 * Class to build strings, with fluent interface
 * @type {String}
 */
class StringBuilder {
  constructor(string = '') {
    this.string = String(string);
  }

  /**
   * Returns the string that has been built
   * @param  {Boolean} [camelCaseIt=false] Whether to camelCase the string
   * @return {String}                      The string that has been built.
   */
  toString(camelCaseIt = false) {
    return camelCaseIt ? this.string.replace(/(\-\w)/g, m => m[1].toUpperCase()) : this.string;
  }

  /**
   * Append a string to the string
   * @param  {String} val the string to append
   * @return {Instance} the instance for chaining
   */
  append(val) {
    this.string += val;
    return this;
  }

  isEmpty() {
    return this.string === '';
  }

  /**
   * Insert a string a particular position
   * @param  {Integer} pos Position at which to insert string
   * @param  {String} val String to insert
   * @return {Instance} the instance for chaining
   */
  insert(pos, val) {
    let length = this.string.length;
    let left = this.string.slice(0, pos);
    let right = this.string.slice(pos);
    this.string = left + val + right;
    return this;
  }
}

let css = `
:host {
 z-index: 5000;
}

#wrapper {
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  display: flex;
  flex-direction: column;
  background: rgba(51, 51, 51, 0.9);
  align-items: center;
  justify-content: center;
  font-family: var(--font-family, verdana);
  z-index: 10000;
}

#popup {
  min-width: 250px;
  max-width: 400px;
  padding: 16px;
  border: 1px solid #ccc;
  background-color: white;
  border-radius: 4px;
  z-index: 10001;
  pointer-events: auto;
}

p {
  margin-top: 0;
}

label {
  display: block;
  margin-bottom: 8px;
}

input[type='text'] {
  border-radius: 4px;
  width: 100%;
  padding: 6px 4px;
  margin: 0;
  box-sizing: border-box;
  border: 1px solid #ccc;
  font-size: var(--input-font-size, 15px);
}

input[type='text'].required {
  border: 1px solid red;
}

button.ok {
  outline: none;
  border-radius: 4px;
  padding: 6px 16px;
  font-size: var(--button-font-size, 15px);
  cursor: pointer;
}

button.cancel {
  outline: none;
  border: 0;
  background-color: transparent;
  padding: 6px 16px;
  font-size: var(--button-font-size, 15px);
  cursor: pointer;
}

.button-group {
  display: flex;
  align-items: center;
  margin: 8px 0 0 0;
}

.hidden {
  display: none !important;
}
`;

let instance;

class WindowDialog extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: 'open' });
    this.cb = Function;
    this.translations = {cancel: 'Cancel', ok: 'OK'};
  }

  alert(message, cb) {
    this.message = message;
    this.type = 'alert';

    this.cb = () => cb();
    this.render();
  }

  confirm(message, cb) {
    this.type = 'confirm';
    this.message = message;
    this.cb = (result) => cb(result);
    this.render();
  }

  connectedCallback() {
    this.render();
  }

  disconnectedCallback() {

  }

  handleEvent(e) {
    let name = e.target.name;
    switch (name) {
      case 'confirm':
        this.cb('ok');
        this.hide();
        break;
      case 'confirm-cancel':
        this.cb('cancel');
        this.hide();
        break;
      case 'prompt':
        let value = this.shadowRoot.querySelector('input[name=prompt]').value;
        if (value && value !== '') {
          this.cb(value);
          this.hide();
        } else {
          this.required = true;
          this.render();
        }
        break;
      case 'promp-cancel':
        this.cb('cancel');
        this.hide();
        break;
      case 'alert':
      default:
        this.cb();
        this.hide();
        break;
    }
  }

  hide(cb) {
    this.type = null;
    this.required = false;
    this.render();
  }

  prompt(message, cb, defaultMessage) {
    this.type = 'prompt';
    this.message = message;
    this.shadowRoot.querySelector('input[name=prompt]').value = null;
    this.promptVal = defaultMessage ? defaultMessage : null;
    this.cb = (result) => cb(result !== '' ? result : null);
    this.render();
  }

  render() {
    hyper(this.shadowRoot) `
      <style>${css}</style>
      <div id="wrapper" class="${this.type === 'alert' || this.type === 'confirm' || this.type === 'prompt' ? '' : 'hidden'}">
        <div id="popup" class="${this.type === 'alert' || this.type === 'confirm' || this.type === 'prompt' ? '' : 'hidden'}">
          <div id="alert" class="${this.type === 'alert' ? '' : 'hidden'}">
            <p>${this.message}</p>
            <div class="button-group">
              <button class="ok" name="alert" onclick="${this}">${this.translations.ok || 'OK'}</button>
            </div>
          </div>
          <div id="confirm" class="${this.type === 'confirm' ? '' : 'hidden'}">
            <p>${this.message}</p>
            <div class="button-group">
              <button class="ok" name="confirm" onclick="${this}">${this.translations.ok || 'OK'}</button>
              <button class="cancel" name="confirm-cancel" onclick="${this}">${this.translations.cancel || 'Cancel'}</button>
            </div>
          </div>
          <div id="prompt" class="${this.type === 'prompt' ? '' : 'hidden'}">
            <p>
              <label for="">${this.message}</label>
              <input type="text" name="prompt" value="${this.promptVal}" class="${this.required ? 'required' : ''}">
            </p>
            <div class="button-group">
              <button class="ok" name="prompt" onclick="${this}">${this.translations.ok || 'OK'}</button>
              <button class="cancel" name="prompt-cancel" onclick="${this}">${this.translations.cancel || 'Cancel'}</button>
            </div>
          </div>
        </div>
      </div>
      `;
  }

  static createDialog(el) {
    if (!instance) {
      customElements.get('window-dialog') || customElements.define('window-dialog', WindowDialog);
      instance = document.createElement('window-dialog');
      if (el instanceof HTMLElement) el.appendChild(instance);
      else document.body.appendChild(instance);
      return instance;
    }
    return instance;
  }

  static get observedAttributes() {
    return [];
  }
}

export { StringBuilder, WindowDialog };
