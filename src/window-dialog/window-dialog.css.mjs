let css = `
:host {
 z-index: 5000;
}

#wrapper {
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  display: flex;
  flex-direction: column;
  background: rgba(51, 51, 51, 0.9);
  align-items: center;
  justify-content: center;
  font-family: var(--font-family, verdana);
  z-index: 10000;
}

#popup {
  min-width: 250px;
  max-width: 400px;
  padding: 16px;
  border: 1px solid #ccc;
  background-color: white;
  border-radius: 4px;
  z-index: 10001;
  pointer-events: auto;
}

p {
  margin-top: 0;
}

label {
  display: block;
  margin-bottom: 8px;
}

input[type='text'] {
  border-radius: 4px;
  width: 100%;
  padding: 6px 4px;
  margin: 0;
  box-sizing: border-box;
  border: 1px solid #ccc;
  font-size: var(--input-font-size, 15px);
}

input[type='text'].required {
  border: 1px solid red;
}

button.ok {
  outline: none;
  border-radius: 4px;
  padding: 6px 16px;
  font-size: var(--button-font-size, 15px);
  cursor: pointer;
}

button.cancel {
  outline: none;
  border: 0;
  background-color: transparent;
  padding: 6px 16px;
  font-size: var(--button-font-size, 15px);
  cursor: pointer;
}

.button-group {
  display: flex;
  align-items: center;
  margin: 8px 0 0 0;
}

.hidden {
  display: none !important;
}
`;

export default css;
