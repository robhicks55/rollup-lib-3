import css from './window-dialog.css.mjs';
import { hyper } from 'hyperhtml';

let instance;

class WindowDialog extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: 'open' });
    this.cb = Function;
    this.translations = {cancel: 'Cancel', ok: 'OK'};
  }

  alert(message, cb) {
    this.message = message;
    this.type = 'alert';

    this.cb = () => cb();
    this.render();
  }

  confirm(message, cb) {
    this.type = 'confirm';
    this.message = message;
    this.cb = (result) => cb(result);
    this.render();
  }

  connectedCallback() {
    this.render();
  }

  disconnectedCallback() {

  }

  handleEvent(e) {
    let name = e.target.name;
    switch (name) {
      case 'confirm':
        this.cb('ok');
        this.hide();
        break;
      case 'confirm-cancel':
        this.cb('cancel');
        this.hide();
        break;
      case 'prompt':
        let value = this.shadowRoot.querySelector('input[name=prompt]').value;
        if (value && value !== '') {
          this.cb(value);
          this.hide();
        } else {
          this.required = true;
          this.render();
        }
        break;
      case 'promp-cancel':
        this.cb('cancel');
        this.hide();
        break;
      case 'alert':
      default:
        this.cb();
        this.hide();
        break;
    }
  }

  hide(cb) {
    this.type = null;
    this.required = false;
    this.render();
  }

  prompt(message, cb, defaultMessage) {
    this.type = 'prompt';
    this.message = message;
    this.shadowRoot.querySelector('input[name=prompt]').value = null;
    this.promptVal = defaultMessage ? defaultMessage : null;
    this.cb = (result) => cb(result !== '' ? result : null);
    this.render();
  }

  render() {
    hyper(this.shadowRoot) `
      <style>${css}</style>
      <div id="wrapper" class="${this.type === 'alert' || this.type === 'confirm' || this.type === 'prompt' ? '' : 'hidden'}">
        <div id="popup" class="${this.type === 'alert' || this.type === 'confirm' || this.type === 'prompt' ? '' : 'hidden'}">
          <div id="alert" class="${this.type === 'alert' ? '' : 'hidden'}">
            <p>${this.message}</p>
            <div class="button-group">
              <button class="ok" name="alert" onclick="${this}">${this.translations.ok || 'OK'}</button>
            </div>
          </div>
          <div id="confirm" class="${this.type === 'confirm' ? '' : 'hidden'}">
            <p>${this.message}</p>
            <div class="button-group">
              <button class="ok" name="confirm" onclick="${this}">${this.translations.ok || 'OK'}</button>
              <button class="cancel" name="confirm-cancel" onclick="${this}">${this.translations.cancel || 'Cancel'}</button>
            </div>
          </div>
          <div id="prompt" class="${this.type === 'prompt' ? '' : 'hidden'}">
            <p>
              <label for="">${this.message}</label>
              <input type="text" name="prompt" value="${this.promptVal}" class="${this.required ? 'required' : ''}">
            </p>
            <div class="button-group">
              <button class="ok" name="prompt" onclick="${this}">${this.translations.ok || 'OK'}</button>
              <button class="cancel" name="prompt-cancel" onclick="${this}">${this.translations.cancel || 'Cancel'}</button>
            </div>
          </div>
        </div>
      </div>
      `;
  }

  static createDialog(el) {
    if (!instance) {
      customElements.get('window-dialog') || customElements.define('window-dialog', WindowDialog);
      instance = document.createElement('window-dialog');
      if (el instanceof HTMLElement) el.appendChild(instance);
      else document.body.appendChild(instance);
      return instance;
    }
    return instance;
  }

  static get observedAttributes() {
    return [];
  }
}

export { WindowDialog };
